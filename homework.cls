\ProvidesClass{homework}[2019/09/17 version 1.0 Homework]
\NeedsTeXFormat{LaTeX2e}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}

\def\@coursename{}
\newcommand{\setcourse}[1]{\def\@coursename{#1}}
\newcommand{\show@course}{\@coursename}

\def\@homeworknumber{}
\newcommand{\sethomework}[1]{\def\@homeworknumber{#1}}
\newcommand{\show@homework}{\@homeworknumber}

\def\@authorname{}
\newcommand{\setauthor}[1]{\def\@authorname{#1}}
\newcommand{\show@author}{\@authorname}

\ProcessOptions \relax

\LoadClass{article}

\RequirePackage[
	left=1in,
	right=1in,
	top=1in,
	bottom=1in,
	headsep=5pt,
	footskip=20pt
]{geometry}

\newcommand\Rom[1]{\MakeUppercase{\romannumeral #1}}

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{\show@course}
\fancyhead[C]{Homework \Rom \show@homework}
\fancyhead[R]{Exercise \show@exercise}
\fancyfoot[L]{\show@author}
\fancyfoot[C]{\today}
\fancyfoot[R]{\thepage}
\renewcommand{\headrulewidth}{.5pt}
\renewcommand{\footrulewidth}{.5pt}

\newmarks\@exercise
\newcommand{\show@exercise}{\botmarks\@exercise}
\newenvironment{ex}[1]{
	\newpage
	\marks\@exercise{#1}
	\begin{framed}
	\noindent
	\textbf{Exercise #1.}
}{
	\end{framed}
}

\usepackage{amsfonts,amsmath}
\usepackage{datetime2}
\usepackage{framed}
